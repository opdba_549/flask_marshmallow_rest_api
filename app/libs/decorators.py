# encoding: utf-8


from functools import wraps
from flask import g, request, jsonify
from sqlalchemy import or_

from app.models.user import User


class ObjectMustBeExist:
    """该装饰器确保操作的对象必须存在
    """

    def __init__(self, object_class=None):
        self.object_class = object_class

    def __call__(self, func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            object_id = kwargs.get('id')

            if not object_id:
                return jsonify({'status': False, 'message': 'object is not exsist'}), 404

            if self.object_class:
                obj = self.object_class.query.get(object_id)
                if not obj:
                    return jsonify({'status': False, 'message': 'object is not exsist'}), 404

                g.instance = obj
            return func(*args, **kwargs)

        return wrapper


class TokenAuthenticate:
    """通过 jwt 认证用户, 验证 HTTP Authorization 头所包含的 token
    """

    def __init__(self, admin=True):
        self.admin = admin

    def __call__(self, func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            pack = request.headers.get('Authorization', None)
            if not pack:
                return jsonify({'status': False, 'message': 'token not found'}), 401

            token_info = pack.split()
            # Authorization 头部值必须为 'jwt <token_value>' 这种形式
            if token_info[0].lower() != 'jwt':
                return jsonify({'status': False, 'message': 'invalid token header'}), 401
            elif len(token_info) == 1:
                return jsonify({'status': False, 'message': 'token missing'}), 401
            elif len(token_info) > 2:
                return jsonify({'status': False, 'message': 'invalid token'}), 401

            # 校验token
            token = token_info[1]
            user = User.verify_token(token)

            # 如果需要验证是否是管理员
            if type(user) == tuple:
                return jsonify({'status': False, 'message': 'Signature has expired'}), 403
            if self.admin and not user.is_admin:
                raise AuthenticationError(403, 'no permission')

            # 将当前用户存入到 g 对象中
            g.user = user
            return func(*args, **kwargs)

        return wrapper


class PaginateSearch:
    """该装饰器封装了分页、搜索、过滤等功能，把查询结果集存入到flask g对象中并返回
        GET /xxoo/?page=x&pagesize=x&search=xxx
        GET /xxoo/?page=x&pagesize=x&search=&name=xxxx
    """

    def __init__(self, object_class=None, search_fields=None):
        self.object_class = object_class
        self.search_fields = search_fields
        self._page = 0  # 页码
        self._pagesize = 0  # 每页显示的行数
        self._search = ''
        self._search_condition = list()

    def __call__(self, func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            try:
                self._page = int(request.args.get('page', 1))
                self._pagesize = int(request.args.get('pagesize', 1000))
                self._search = request.args.get('search', '')
            except Exception as e:
                raise (403, str(e))

            if self._search:
                # 拼接搜索条件
                for field in self.search_fields:
                    condition = "self.object_class.{}.contains(self._search)".format(
                        field)
                    self._search_condition.append(eval(condition))

                # 分页
                paginate = self.object_class.query.filter(or_(*self._search_condition)) \
                    .order_by(self.object_class.id.desc()) \
                    .paginate(self._page, self._pagesize, error_out=False)
                count = self.object_class.query.filter(
                    *self._search_condition).count()
                queryset = paginate.items
            # 在没有搜索并且没有任何查询参数的情况
            elif len(request.args) == 0:
                queryset = self.object_class.query.all()
                count = self.object_class.query.count()
            else:
                paginate = self.object_class.query.order_by('id').paginate(
                    self._page, self._pagesize, error_out=False)
                queryset = paginate.items
                count = self.object_class.query.count()

            g.instance = {
                "queryset": queryset,
                "count": count
            }

            return func(*args, **kwargs)

        return wrapper
