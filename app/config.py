# encoding: utf-8

import os

BASE_DIR = os.path.dirname(os.path.abspath(__file__))


class BaseConfig:
    TEMPLATES_AUTO_RELOAD = True
    DEBUG = False
    SECRET_KEY = 'mlhrjuv0qz1mm=cfinicja1dSzjbtcwvpvyf4vo0zss5vvcC44'
    DOMAIN = 'xxoo.com'


class DevConfig(BaseConfig):
	DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'mysql://root:123456@127.0.0.1:3306/flask_marshmallow_template?charset=utf8'
    SQLALCHEMY_TRACK_MODIFICATIONS = True


class ProductConfig(BaseConfig):
    SQLALCHEMY_DATABASE_URI = 'mysql://root:123456@127.0.0.1:3306/dbms?charset=utf8'
