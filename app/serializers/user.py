# encoding: utf-8


from marshmallow import (fields, validate, post_load,
                         validates_schema, ValidationError)

from app.models.user import User, Role
from app.extensions import db, ma


class UserSchema(ma.ModelSchema):
    login_at = fields.DateTime(dump_only=True, format='%Y-%m-%d %H:%M:%S')
    password = fields.String(load_only=True, validate=validate.Length(5, 20))
    roles = fields.Nested('RoleSchema', many=True)

    class Meta:
        model = User
        fields = ('id', 'username', 'realname', 'email',
                  'is_admin', 'password', 'roles')

    @validates_schema
    def validate_schema(self, data):
        """检查数据是否存在，存在则抛出异常，返回给前端
        """

        # 拿到当前操作的对象（put/delete方法调用）
        instance = self.context.get('instance', None)

        # 检查数据是否已经存在
        user = User.query.filter(db.or_(User.username == data.get(
            'username'), User.email == data.get('email'))).first()

        if not user:
            return

        # post请求时调用
        if instance is None:
            raise ValidationError(
                '{} user already exist'.format(user.username))

    @post_load
    def make_instance(self, data):
        """数据加载成功后自动创建
        """
        # 拿到当前操作的对象（put/delete方法调用）
        instance = self.context.get('instance', None)

        # 创建实例对象，并返回给视图函数的post方法，最后在视图post方法中调用save
        if instance is None:
            return User(**data)

        # 循环更新字段
        for key in data:
            setattr(instance, key, data[key])

        return instance


class RoleSchema(ma.ModelSchema):
    user_list = fields.List(fields.String())

    class Meta:
        model = Role
        fields = ('id', 'name', 'user_list')

    @validates_schema
    def validate_schema(self, data):

        instance = self.context.get('instance', None)

        role = Role.query.filter(Role.name == data.get("name")).first()
        if not role:
            return

        if instance is None:
            raise ValidationError('{} role already exist'.format(role.name))

    @post_load
    def make_instance(self, data):

        instance = self.context.get('instance', None)

        if instance is None:
            return Role(**data)

        for key in data:
            setattr(instance, key, data[key])

        return instance
