"""模型基类
"""

from datetime import datetime
from sqlalchemy.orm import class_mapper

from app.extensions import db


def model_to_dict(model):
    """将model对象转换成字典
    """
    model_dict = {}
    for key, column in class_mapper(model.__class__).c.items():
        model_dict[column.name] = getattr(model, key, None)
    return model_dict


def model_to_dict_v2(model, fields=None):
    """将model对象转换成字典，并排除不需要转换的字段
    """
    model_dict = {}
    column_dict = class_mapper(model.__class__).c
    if not fields:
        fields = list()
    for key, column in column_dict.items():
        model_dict[column.name] = getattr(model, key, None)
        if key in fields:
            model_dict.pop(key)
    return model_dict


class BaseModel(db.Model):
    """通过设置 __abstract__ 属性，成为 SQLAlchemy 抽象基类，不再映射到数据库中
    """
    __abstract__ = True

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    create_time = db.Column(
        db.DateTime, default=datetime.now(), comment="创建时间")
    update_time = db.Column(
        db.DateTime, default=datetime.now(), comment="更新时间")
    remark = db.Column(db.String(256), default='', comment="备注")

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()
