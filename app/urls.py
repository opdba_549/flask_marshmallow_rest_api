"""定义所有 API 对应的 URL
"""

from flask import Blueprint

from app.views.index import IndexView
from app.views.user import UserView, UserDetailView, RoleView, RoleDetailView
from app.views.auth import AuthView


# 实例化蓝图
api = Blueprint('api', __name__)


# 主页
api.add_url_rule('/api/', view_func=IndexView.as_view('index'),
                 strict_slashes=False)

# 登录
api.add_url_rule(
    '/api/login/', view_func=AuthView.as_view('login'), strict_slashes=False)

# 用户
api.add_url_rule(
    '/api/users/', view_func=UserView.as_view('user'), strict_slashes=False)
api.add_url_rule('/api/users/<int:id>/',
                 view_func=UserDetailView.as_view('user_detail'), strict_slashes=False)

api.add_url_rule(
    '/api/roles/', view_func=RoleView.as_view('role'), strict_slashes=False)
api.add_url_rule(
    '/api/roles/<int:id>', view_func=RoleDetailView.as_view('role_detail'), strict_slashes=False)