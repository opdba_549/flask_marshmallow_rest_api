"""
该模块以工厂模式实现 app 创建函数
"""

from flask import Flask


from app.config import DevConfig, ProductConfig
from app.extensions import init_ext
from app.urls import api


def create_app():
    app = Flask(__name__)

    import pymysql
    pymysql.install_as_MySQLdb()

    # 加载配置文件
    app.config.from_object(DevConfig)

    # 初使化第三方扩展插件
    init_ext(app=app)

    # 加载模型, 告诉flask app实例对象 model 在哪
    from app import models

    # 注册蓝图，加载路由
    app.register_blueprint(api)

    return app
