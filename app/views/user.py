# encoding: utf-8


from flask.views import MethodView
from flask import jsonify, request, g
from werkzeug.security import generate_password_hash, check_password_hash

from app.models.user import User, Role
from app.models.base import model_to_dict, model_to_dict_v2
from app.extensions import db
from app.libs.decorators import ObjectMustBeExist, TokenAuthenticate, PaginateSearch
from app.serializers.user import UserSchema, RoleSchema


class UserView(MethodView):

    search_fields = ["username"]
    decorators = [TokenAuthenticate(), PaginateSearch(User, search_fields)]

    def get(self):
        users = g.instance.get('queryset')
        count = g.instance.get('count')

        # for user in users:
        #     # user.role_list = [role.name for role in user.roles]

        data = UserSchema().dump(users, many=True).data

        result = dict(
            data=data,
            count=count
        )

        return jsonify({'status': True, 'data': result})

    def post(self):
        data = request.get_json()
        # 通过前端传来的role，得到role对象
        role = data.pop('role')
        role_obj = Role.query.filter(Role.name == role).first()

        user, errors = UserSchema().load(data)
        if errors:
            return jsonify({'status': True, 'message': errors}), 201

        # 生成用户密码
        user.password = data.get('password', '123456')
        # 生成用户邮箱
        user.email = data['username'] + '@xxoo.com'
        # 将用户添加到角色
        user.roles.append(role_obj)
        user.save()

        return jsonify({'status': True, 'message': 'success'}), 201


class UserDetailView(MethodView):

    # 装饰器，api资源请求时调用
    decorators = [TokenAuthenticate(), ObjectMustBeExist(User)]

    def get(self, id):
        data, _ = UserSchema().dump(g.instance)
        return jsonify({'status': True, 'data': data})

    def delete(self, id):
        g.instance.delete()
        return jsonify({'status': True, 'message': 'success'}), 201

    def put(self, id):
        schema = UserSchema(context={'instance': g.instance})
        data = request.get_json()
        user, error = schema.load(data, partial=True)
        if error:
            return jsonify({'status': False, 'message': error}), 400
        user.save()

        return jsonify({'status': True, 'message': 'success'}), 201


class RoleView(MethodView):

    search_fields = ["name"]
    decorators = [TokenAuthenticate(), PaginateSearch(Role, search_fields)]

    def get(self):
        roles = g.instance.get('queryset')
        count = g.instance.get('count')

        for role in roles:
            # role.user_list中的user_list是序列化类中定义的字段
            role.user_list = [user.username for user in role.users.all()]

        data = RoleSchema().dump(roles, many=True).data

        result = dict(
            data=data,
            count=count
        )

        return jsonify({'status': True, 'data': result})

    def post(self):
        data = request.get_json()
        role, errors = RoleSchema().load(data)
        if errors:
            return jsonify({'status': True, 'message': 'role already exist'}), 201
        role.save()
        return jsonify({'status': True, 'message': 'success'}), 201


class RoleDetailView(MethodView):

    decorators = [TokenAuthenticate(), ObjectMustBeExist(Role)]

    def get(self, id):
        role_obj = g.instance
        data, _ = RoleSchema().dump(role_obj)

        # 显示当前role的用户列表
        user_list = [user.username for user in role_obj.users.all()]
        data['users'] = user_list
        return jsonify({'status': True, 'data': data})

    def put(self, id):
        schema = RoleSchema(context={'instance': g.instance})
        data = request.get_json()
        role, error = schema.load(data, partial=True)
        if error:
            return jsonify({'status': False, 'message': error}), 400
        role.save()

        return jsonify({'status': True, 'message': 'success'}), 201

    def delete(self, id):
        g.instance.delete()
        return jsonify({'status': True, 'message': 'success'}), 204
