# Flask_Marshmallow_template

#### 介绍
```
基于 Flask 结合 Marshmallow 实现的后端开发简易模板，适用于前后端分离模式，遵循rest api风格。
本套开发模板给出了 用户模块 的简单案例。
```



#### 项目目录结构
```
├── app                     # 项目app总目录
│   ├── __init__.py         # 项目初使化文件
│   ├── config.py           # 配置文件   
│   ├── extensions.py       # 第三方扩展插件
│   ├── libs                # 共用类，函数包
│   ├── models              # 数据库模型包
│   ├── urls.py             # 视图路由url文件
│   └── views               # 业务处理视图包
├── manage.py               # 程序入口文件
├── readme.md
├── requirements.txt

```

#### 安装教程
```
1. 安装开发环境
git clone https://gitee.com/opdba_549/flask_marshmallow_rest_api/
cd flask_marshmallow_template
python3 -m venv myenv
source myenv/bin/activate

注意：python版本3.5+

2. 安装开发依赖
pip install -r requirements.txt

3. 修改config.py配置
修改数据库相关的配置，并创建好数据库

4. 初使化数据库
python manage.py init
python manage.py migrate
python manage.py upgrade

5. 创建一个管理员账号
python manage.py create_admin -u admin -p admin

6. 启动开发环境
python manage.py runserver -h 127.0.0.1 -p 5000

```
